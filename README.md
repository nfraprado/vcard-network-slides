# vCard Network Slides

Presentation about [vCard
Network](https://codeberg.org/nfraprado/vcard-network).

Generate the presentation from the Markdown source using
[Marp](https://github.com/marp-team/marp) with

```
npx @marp-team/marp-cli@latest slides.md -o slides.html
```

for HTML, or

```
npx @marp-team/marp-cli@latest slides.md -o slides.pdf --allow-local-files
```

for PDF.

## License

CC-BY-4.0
