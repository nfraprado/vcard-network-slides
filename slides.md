<!---
Outline:
Motivation
How it works
- rss/atom + vcard
Demo
Links:
https://codeberg.org/nfraprado/vcard-network

QRCode with my profile
--->

# vCard Network

---

## Motivation

* Allow people to connect with me despite me not having social media
* Promote data ownership for personal connections rather than accept vendor lock-in

---

## How it works

* Combination of two widely used formats:
  * vCard
    * Format for electronic business card, widely used for contacts. cardDAV allows synchronization
    * Contains person's information: name, photo, organization, address, etc
    * Acts as the person's profile
  * RSS/Atom feed
    * Provides updates from a website
    * Acts as the person's feed

---

## Demo

* See profile and connections display in action [on my website](https://nfraprado.net/pages/vcard-network.html)
* Example OPML export can be showcased on https://demo.tt-rss.org

---

## Try it out

* Check out the repository:
  https://codeberg.org/nfraprado/vcard-network
  ![h:190](img/repo-qrcode.png)

* Connect with me:
  https://nfraprado.net/profile.vcf
  ![h:190](img/profile-qrcode.png)

<!---
marp notes:
can't wrap lines, that's reflected on the output
--->
